document.addEventListener('DOMContentLoaded', function () {
    renderProductItems();
});


const items = [
    {
        id: 1,
        name: "Nồi Chiên Không Dầu OLIVO AF15 - 16",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/f9/55/b7/ddee5e10fd88cbb0b6fa4b400494a53a.jpg.webp",
        quanlity: 483,
        price: "4.190.000",
        discount: 20,
        star: 3
    },
    {
        id: 2,
        name: "Máy Tính Bảng OPPO Pad Air (4GB/64GB) | Màn Hình 2K 1 tỷ Màu | Chip Snapdragon 680 | Hàng Chính Hãng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/86/6c/d5/9a30dd04b4f31fa48fbd6f40645cca53.jpg",
        quanlity: 483,
        price: "6.990.000",
        discount: 20,
        star: 5
    },
    {
        id: 3,
        name: "Ly nhựa 2 lớp LocknLock kèm ống hút HAP507 750ml",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/21/d7/bc/b80fd5aa548700350c3e039f61c13d4d.jpg",
        quanlity: 483,
        price: "109.000",
        discount: 50,
        star: 3
    },
    {
        id: 4,
        name: "Gối Cao Su Liên Á Contour gợn sóng, thoáng mát, hạn chế ngáy ngủ",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/45/63/a0/1dd56d7435bd1b86b401b23468e45957.jpg",
        quanlity: 483,
        price: "249.750",
        discount: 8,
        star: 3
    },
    {
        id: 5,
        name: "Gối Memory Foam 50D Hình Cong Lock&Lock HLW111 (50 x 30 cm) - Trắng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/e4/39/33/bbb59a69c4f20dcfdc227b58aab7ee27.jpg",
        quanlity: 483,
        price: "433.000",
        discount: 10,
        star: 3
    },
    {
        id: 6,
        name: "Tai Nghe Bluetooth - Tai Nghe Nhét Tai Không Dây TW16 - Cảm Ứng Vân Tay - Chống ồn - Chống nước - Kết Nối Bluetooth 5.0 - Âm Thanh HiFi",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/ae/ba/70/ffb982bd9e451c18be7886ec1e8731aa.jpg",
        quanlity: 483,
        price: "239.000",
        discount: 13,
        star: 3
    },
    {
        id: 7,
        name: "Tủ Lạnh 2 Cánh Panasonic 322 Lít NR-BC360WKVN ngăn đá dưới - Lấy nước ngoài - Hàng chính hãng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/37/ca/37/21a27df38f0d1b15f3dbeb7515ca0dea.png",
        quanlity: 483,
        price: "17.090.000",
        discount: 13,
        star: 3
    },
    {
        id: 8,
        name: "Apple iPhone 11",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/aa/24/e0/57914989351786ef9dfe027046ba4626.jpg",
        quanlity: 1000,
        price: "10.850.000",
        discount: 16,
        star: 3
    },
    {
        id: 9,
        name: "Gối Memory Foam 50D Hình Cong Lock&Lock HLW111 (50 x 30 cm) - Trắng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/89/37/ff/16d7ccefa742ec7dcb443fe8b38fd025.jpg",
        quanlity: 1000,
        price: "27.550.000",
        discount: 11,
        star: 3
    },
    {
        id: 10,
        name: "Điện Thoại Oppo Reno 6Z 5G (8GB/128G) - Hàng Chính Hãng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/0d/f4/c6/ca9484b98ecce0a5c65273e4203a010c.jpg",
        quanlity: 1000,
        price: "7.390.000",
        discount: 17,
        star: 3
    },
    {
        id: 11,
        name: "[Lắp đặt trong vòng 24h] Máy Giặt Cửa Trước Panasonic 11 Kg NA-V11FX2LVT- Diệt Vi Khuẩn 99.9% - Hàng chính hãng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/54/76/7e/98effd61f685f7df266037e838062445.png",
        quanlity: 1000,
        price: "18.990.000",
        discount: 11,
        star: 3
    },
    {
        id: 12,
        name: "Tủ Kệ Sách Đứng Đa Năng 4 Tầng Phong Cách BAYA Sund Chất Liệu Gỗ MFC Bền Chắc Thiết Kế Hình Chữ Nhật Nhiều Màu Sắc Hiện Đại Tối Giản",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/f0/9c/68/67c6b632ff63fdd315962025913a35c7.png",
        quanlity: 1000,
        price: "999.000",
        discount: 11,
        star: 3
    },
    {
        id: 13,
        name: "Điện Thoại Oppo Reno 7 5G (8GB/256G) - Hàng Chính Hãng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/21/7a/7d/39a8bae7367459bde5c287eb9902f9c9.jpg",
        quanlity: 1000,
        price: "10.990.000",
        discount: 11,
        star: 3
    },
    {
        id: 14,
        name: "Tai nghe bluetooth không dây F9 True wireless Dock Sạc có Led Báo Pin Kép",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/28/d6/e7/313017975b3219a684abcc929f0daa56.jpg",
        quanlity: 1000,
        price: "88.990",
        discount: 11,
        star: 3
    },
    {
        id: 15,
        name: "Tai Nghe Bluetooth Nhét Tai Không Dây Inpods Pro 13 TWS Thế Hệ Nâng Cấp. ",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/33/59/bb/bc681041e08cb85215e40b0069f6c22f.jpg",
        quanlity: 1000,
        price: "159.000",
        discount: 11,
        star: 3
    },
    {
        id: 16,
        name: "Gối Memory Foam 50D Hình Cong Lock&Lock HLW111 (50 x 30 cm) - Trắng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/89/37/ff/16d7ccefa742ec7dcb443fe8b38fd025.jpg",
        quanlity: 1000,
        price: "27.550.000",
        discount: 11,
        star: 3
    },
    {
        id: 17,
        name: "Tai nghe Bluetooth M9, tai nghe không dây cảm ứng thông minh. âm thanh HiFi trung thực, màn hình hiển thị sắc nét, tích hợp thêm đèn pin soi sáng- Hàng nhập khẩu",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/cf/ad/7c/723babe57cfc2ac35cf12dabbf1c8bec.jpg",
        quanlity: 1000,
        price: "27.550.000",
        discount: 11,
        star: 3
    },
    {
        id: 18,
        name: "Giường Ngủ Đơn BAYA SAPA Kích Thước L194xW97xH30 Làm Từ Gỗ Keo Với Độ Bền Cao Màu Trắng Trang Nhã 7100038",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/13/09/28/04760eae9e370f0d38748999897c2c47.jpg",
        quanlity: 1000,
        price: "1.290.000",
        discount: 11,
        star: 3
    },
    {
        id: 19,
        name: "Ly, Cốc Gỗ Uống Trà Phong Cách Nhật Làm Thủ Công",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/42/38/12/c8defededbdafde8deb1d941f6bfd771.jpg",
        quanlity: 1000,
        price: "140.000",
        discount: 11,
        star: 3
    },
    {
        id: 20,
        name: "[CHỈ GIAO TẠI HCM] - Máy giặt Panasonic Inverter 9.5 Kg NA-FD95X1LRV- Hàng Chính Hãng",
        img: "https://salt.tikicdn.com/cache/280x280/ts/product/e1/f9/e3/55d9ff752d260ddea1ccd91fa22e3134.png",
        quanlity: 1000,
        price: "16.590.000",
        discount: 11,
        star: 3
    },
]

function renderProductItems() {
    let html = '';
    let itemsLength = items.length;
    for (let i = 0; i < itemsLength; i++) {
        const item = items[i];
        html += `
        <div class="col l-2 m-6 c-12">
        <a class="product-item"><span class="item-description">
                <div>
                    <div class="thumbnail">
                        <img src="https://salt.tikicdn.com/ts/upload/dc/0d/49/ef9dc5d8164bd62b011e54276502b342.png"
                            width="136" height="24" class="freeship-icon"
                            style="bottom: 0px; left: 0px;">
                        <div class="">
                            <img src="${item.img}"
                                alt="${item.name}" />
                        </div>
                    </div>
                    <div class="info">
                        <div class="badge-service"></div>
                        <div class="name">
                            <h3>${item.name}</h3>
                        </div>
                        <div style="display: flex; align-items: center;">
                            <div style="display: flex;">
                                <div style="position: relative;">
                                    <div style="display: flex; align-items: center;">
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#c7c7c7" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(199, 199, 199);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#c7c7c7" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(199, 199, 199);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#c7c7c7" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(199, 199, 199);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#c7c7c7" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(199, 199, 199);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#c7c7c7" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(199, 199, 199);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                    </div>
                                    <div
                                        style="width: 99%; white-space: nowrap; position: absolute; left: 0px; top: 0px; overflow: hidden;">
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#fdd836" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(253, 216, 54);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#fdd836" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(253, 216, 54);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#fdd836" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(253, 216, 54);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#fdd836" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(253, 216, 54);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                        <svg stroke="currentColor" fill="currentColor"
                                            stroke-width="0" viewBox="0 0 24 24" size="14"
                                            color="#fdd836" height="14" width="14"
                                            xmlns="http://www.w3.org/2000/svg"
                                            style="color: rgb(253, 216, 54);">
                                            <path
                                                d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z">
                                            </path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div
                                style="display: flex; align-items: center; color: rgb(120, 120, 120); line-height: normal; font-size: 11px; padding-left: 4px;">
                                <div
                                    style="width: 1px; height: 9px; background-color: rgb(199, 199, 199);">
                                </div>
                                <div style="padding-left: 6px;">Đã
                                    bán ${item.quanlity}</div>
                            </div>
                        </div>
                        <div class="price-discount has-discount">
                            <div class="price-discount__price">${item.price} ₫</div>
                            <div class="price-discount__discount">- ${item.discount}%</div>
                        </div>
                        <div class="badge-under-price"></div>
                        <div class="badge-benefits"></div>
                        <div class="badge-additional-info"></div>
                    </div>
                </div>
            </span>
        </a>
    </div>
        `
    }

    var listProduct = document.getElementById('list-product-item');
    listProduct.innerHTML = html;
}